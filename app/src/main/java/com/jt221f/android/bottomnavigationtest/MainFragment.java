package com.jt221f.android.bottomnavigationtest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainFragment extends Fragment {
    private ViewPager mViewPager;
    private BottomNavigationView bottomNavigationView;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            bottomNavigationView.getMenu().setGroupCheckable(0, true, true);
            final BadgeDrawable badgeDrawable = bottomNavigationView.getOrCreateBadge(item.getItemId());
            if (badgeDrawable != null) {
                int number = badgeDrawable.getNumber();
                number++;
                badgeDrawable.setNumber(number);
            }

            final TestPagerAdapter adapter = (TestPagerAdapter)mViewPager.getAdapter();
            final int index = adapter.getItemIndex(item.getItemId());
            if (index != -1) {
                mViewPager.setCurrentItem(index, false);
                return true;
            }
            return false;
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bottomNavigationView = view.findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        setBottomNavigationView(bottomNavigationView);


        mViewPager = view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new TestPagerAdapter(getContext(), getFragmentManager(), bottomNavigationView));
        mViewPager.setCurrentItem(0, false);

        bottomNavigationView.getMenu().setGroupCheckable(0, false, true);
    }

    protected void setBottomNavigationView(BottomNavigationView bottomNavigationView) {
        // タブ切り替え時のアニメーションをdisableにする
        final BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        for (int i = 0 ; i < menuView.getChildCount() ; i++) {
            final BottomNavigationItemView itemView = (BottomNavigationItemView)menuView.getChildAt(i);
            itemView.setShifting(false);
            itemView.setChecked(false);
        }
    }
}
