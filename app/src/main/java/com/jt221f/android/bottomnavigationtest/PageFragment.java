package com.jt221f.android.bottomnavigationtest;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class PageFragment extends Fragment {
    private static final String DEFAULT_TITLE = "Unknown";
    public static final String KEY_TITLE = "title";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_page, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView textView = view.findViewById(R.id.textview);
        textView.setText(getTitle());
    }

    private String getTitle() {
        String title = DEFAULT_TITLE;
        final Bundle args = getArguments();
        if (args != null) {
            title = args.getString(KEY_TITLE, DEFAULT_TITLE);
        }
        return title;
    }
}
