package com.jt221f.android.bottomnavigationtest;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class TestPagerAdapter extends FragmentPagerAdapter {
    private List<ItemInfo> listData = new ArrayList<>();
    private Context context;
    public TestPagerAdapter(Context context, FragmentManager fragmentManager, BottomNavigationView bottomNavigationView) {
        super(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.context = context;
        setData(bottomNavigationView);
    }

    private void setData(BottomNavigationView bottomNavigationView) {
        final Menu menu = bottomNavigationView.getMenu();
        for (int i = 0 ; i < menu.size() ; i++) {
            final MenuItem menuItem = menu.getItem(i);
            final ItemInfo itemInfo = new ItemInfo();
            itemInfo.id = menuItem.getItemId();
            itemInfo.title = menuItem.getTitle().toString();
            listData.add(itemInfo);
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        final PageFragment fragment = new PageFragment();
        final Bundle args = new Bundle();
        final ItemInfo info = listData.get(position);
        args.putString(PageFragment.KEY_TITLE, info.title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return (listData == null ? 0 : listData.size());
    }

    public int getItemIndex(int itemId) {
        for (int i = 0 ; i < listData.size() ; i++) {
            final ItemInfo info = listData.get(i);
            if (info.id == itemId) {
                return i;
            }
        }
        return -1;
    }

    class ItemInfo {
        public int id;
        public String title;
    }
}
