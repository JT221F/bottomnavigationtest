package com.jt221f.android.bottomnavigationtest;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class ViewPagerEx extends ViewPager {
    private boolean isSwipeEmnabled = false;
    public ViewPagerEx(@NonNull Context context) {
        super(context);
    }

    public ViewPagerEx(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSwipeEmnabled(boolean isSwipeEmnabled) {
        this.isSwipeEmnabled = isSwipeEmnabled;
    }

    public boolean isSwipeEmnabled() {
        return this.isSwipeEmnabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return (isSwipeEmnabled ? super.onInterceptTouchEvent(ev) : false);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return (isSwipeEmnabled ? super.onTouchEvent(ev) : false);
    }
}
